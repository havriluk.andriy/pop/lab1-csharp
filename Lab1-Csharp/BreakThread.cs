using System.Runtime.ConstrainedExecution;

namespace cnu.andriyh.pop.lab1
{
    class BreakThread {
        private readonly int sleepTime;
        private volatile Permission permission = new Permission();

        public BreakThread(int sleepTime)
        {
            this.sleepTime = sleepTime * 1000;
        }

        public void Run()
        {
            try
            {
                Thread.Sleep(sleepTime);
            }
            catch (ThreadInterruptedException)
            {
                throw new Exception("Керуючий потік перервано!");
            }

            lock (permission)
            {
                permission.CanStop = true;
            }
        }

        public Permission GetPermission()
        {
            return this.permission;
        }

        public class Permission
        {
            public bool CanStop { get; set; } = false;
        }
    }   
}