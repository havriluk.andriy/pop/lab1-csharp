namespace cnu.andriyh.pop.lab1
{
    class Runner
    {
        private readonly BreakThread control;
        private readonly string name;

        public Runner(string name, BreakThread control)
        {
            this.name = name;
            this.control = control;
        }

        public void Run()
        {
            long sum = 0;
            long steps = 0;

            while (!control.GetPermission().CanStop)
            {
                sum += 2;
                steps++;
            }

            Console.WriteLine($"Потік: [{name}] зупинено з сумою: {sum}, кількістю кроків: {steps}");
        }
    }
}