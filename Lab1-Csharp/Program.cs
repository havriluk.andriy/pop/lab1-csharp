﻿namespace cnu.andriyh.pop.lab1
{
    class Program 
    {
        static void Main(string[] args) 
        {
            if (args.Length != 2) 
            {
                Console.WriteLine("Відсутні аргументи програми - кількість потоків та час очікування в секундах");
                return;
            }
            int threads = int.Parse(args[0]);
            int time = int.Parse(args[1]);

            BreakThread breakThread = new BreakThread(time);
            Console.WriteLine("Запускаємо потоки...");
            for (int i = 0; i < threads; i++) {
                new Thread(new Runner("-" + (i+1) + "-", breakThread).Run).Start();
            }

            new Thread(breakThread.Run).Start();
            Console.WriteLine("Потоки запущено, очікуйте...\n");
        }
    }
}